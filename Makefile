.PHONY: all clean watch help deploy

HUGO := hugo
FIREBASE := firebase
PROJECT := bdimcheff
WEBSITE_URL := brandon.dimcheff.com

# All input files
FILES=$(shell find config content archetypes static themes -type f)

# Below are PHONY targets
all: public

help:
	@echo "Usage: make <command>"
	@echo "  all     Builds the blog and minifies it"
	@echo "  clean   Cleans all build files"
	@echo "  watch   Runs hugo in watch mode, waiting for changes"
	@echo ""
	@echo "New article:"
	@echo "  hugo [-k kind] new post/the_title"
	@echo "  $$EDITOR content/post/the_title.md"
	@echo "  make watch"
	@echo "  open "

clean:
	-rm -rf public

watch: clean
	$(HUGO) server -w

# Below are file based targets
public: $(FILES)
	$(HUGO)

	# process files here if necessary

	# Ensure the public folder has it's mtime updated.
	touch $@

deploy: public
	$(FIREBASE) --project $(PROJECT) deploy
