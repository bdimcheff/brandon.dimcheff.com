+++
# Display name
name = "Brandon Dimcheff"

# Username (this should match the folder name)
authors = ["admin"]

# Is this the primary user of the site?
superuser = true

# Role/position
role = "Chief Architect"

# Organizations/Affiliations
#   Separate multiple entries with a comma, using the form: `[ {name="Org1", url=""}, {name="Org2", url=""} ]`.
organizations = [ { name = "Openly", url = "https://www.openly.com" } ]

# Short bio (displayed in user profile at end of posts)
bio = "Brandon Dimcheff is a software engineer born and raised in Ann Arbor, Michigan. He uses go for his day job, has fallen in love with Kubernetes, is an aspiring functional programming language nerd, and is an advocate of open source."

# Enter email to display Gravatar (if Gravatar enabled in Config)
email = ""

# List (academic) interests or hobbies
interests = [
  "Distributed Systems",
  "Kubernetes",
  "Functional Programming",
  "Skiing",
  "Cooking",
  "Michigan Football and Hockey"
]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "BSE in Computer Science"
  institution = "University of Michigan"
  year = 2005


# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups = ["Orchestructure"]

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.


[[social]]
  icon = "envelope"
  icon_pack = "fas"
  link = "mailto:brandon@dimcheff-removethis.com"

[[social]]
  icon = "twitter"
  icon_pack = "fab"
  link = "//twitter.com/bdimcheff"

[[social]]
  icon = "github"
  icon_pack = "fab"
  link = "//github.com/bdimcheff"

[[social]]
  icon = "gitlab"
  icon_pack = "fab"
  link = "//gitlab.com/bdimcheff"

[[social]]
  icon = "linkedin"
  icon_pack = "fab"
  link = "//linkedin.com/in/bdimcheff"

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# [[social]]
#   icon = "cv"
#   icon_pack = "ai"
#   link = "files/cv.pdf"

+++

Brandon Dimcheff is a software engineer born and raised in Ann Arbor, Michigan. He uses go for his day job, has fallen in love with Kubernetes, is an aspiring functional programming language nerd, and is an advocate of open source. He's a [pretty good cook](/recipe) and tries to spend a lot of time knee deep in powder.

He's currently the Chief Architect at [Openly](https://www.openly.com), an insuretech startup. He lives in Ann Arbor, Michigan with his wife, Danielle, and his two cats, Marc and Cleo.
