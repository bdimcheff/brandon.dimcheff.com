+++
# About/Biography widget.

date = "2016-04-20T00:00:00"
author = "admin"
draft = false

widget = "about"

# Order that this section will appear in.
weight = 5
headless = true  # Homepage is headless, other widget pages are not.

+++

# Biography

Brandon Dimcheff is a software engineer born and raised in Ann Arbor, Michigan. He uses go for his day job, has fallen in love with Kubernetes, is an aspiring functional programming language nerd, and is an advocate of open source. He's a [pretty good cook](/recipe) and tries to spend a lot of time knee deep in powder.

He's currently the Chief Architect at [Openly](https://www.openly.com), an insuretech startup. He lives in Ann Arbor, Michigan with his wife, Danielle, and his two cats, Marc and Cleo.
