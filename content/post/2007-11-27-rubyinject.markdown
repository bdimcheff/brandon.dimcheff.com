---
author: brandon.dimcheff
date: 2007-11-27T00:00:00Z
title: RubyInject
url: /2007/11/27/rubyinject/
tags: []
---

[RubyInject](http://rubycocoa.sourceforge.net/RubyInject) is a small library from the folks who brought you RubyCocoa.  It allows you to inject a ruby interpreter into a Cocoa app and tinker with all its' internals.
