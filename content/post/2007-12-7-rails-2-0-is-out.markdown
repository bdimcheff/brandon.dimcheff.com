---
author: brandon.dimcheff
date: 2007-12-07T00:00:00Z
title: Rails 2.0 is out
url: /2007/12/07/rails-2-0-is-out/
tags: []
---

[Rails 2.0](http://weblog.rubyonrails.org/2007/12/7/rails-2-0-it-s-done) has been released. Check it.
