---
author: brandon.dimcheff
date: 2007-08-27T00:00:00Z
title: First Post
url: /2007/08/27/first-post/
tags: []
---

After 9 months of trying to get a Ruby blog launched, I finally have succeeded.  And now it's late, so I'm going to bed.  But check back soon for juicy Ruby and Rails tidbits.

No, tags don't quite work yet.  It's Heminway's fault.  I'm fixing it and I'll release my changes.
