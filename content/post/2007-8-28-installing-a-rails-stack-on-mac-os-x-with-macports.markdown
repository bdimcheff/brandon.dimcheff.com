---
author: brandon.dimcheff
date: 2007-08-28T00:00:00Z
title: Installing a Rails stack on Mac OS X with MacPorts
url: /2007/08/28/installing-a-rails-stack-on-mac-os-x-with-macports/
tags: []
---

I have a clean install of OS X on which I need to install Rails.  I've done this maybe 3 or 4 times in the past year with minor variations each time which inevitably come back to bite me later.  This post is an effort to document and standardize all the details to get a Rails stack going with MacPorts.

## Installing Xcode Tools
You need the latest version of [Xcode tools](http://developer.apple.com/tools/xcode/) to be able to compile all the packages necessary for Ruby and Rails.  They're a free download from Apple.

## Installing MacPorts
First thing's first: Install [MacPorts](http://trac.macosforge.org/projects/macports/wiki/InstallingMacPorts)! Be sure to follow the directions closely.  Especially the bit about setting your `PATH` appropriately.  The /opt paths must be before your standard path so when you type `ruby`, you use the ruby interpreter from MacPorts rather than the old one that came with OS X.

## Installing Rails Prerequisites with MacPorts
    $ sudo port install mysql5 +server
    $ sudo port install ruby
    $ sudo port install rb-rubygems
    $ sudo port install rb-mysql
    $ sudo port install rb-termios
    $ sudo port install subversion +tools

## Configure MySQL
First, add `/opt/local/lib/mysql5/bin/` to your `PATH` as you did in the instructions to install MacPorts.

    $ sudo mysql_install_db5 --user=mysql

## Install Gems
    $ sudo gem install -y rake
    $ sudo gem install -y rails
    $ sudo gem install -y capistrano
    $ sudo gem install -y mongrel
    $ sudo gem install -y mongrel_cluster
    $ sudo gem install -y piston

## Install Optional Packages
The above ports and gems are the minimum you'll need to get rails running successfully.  The ones below, while not required, are often useful when developing rails apps.

### Other Databases
Try postgres or sqlite3 for some database variety (`port install postgresql82-server && port install rb-postgres` or `port install rb-sqlite3`, respectively).

### Testing
 * [RSpec](http://rspec.rubyforge.org/) - a powerful [BDD](http://en.wikipedia.org/wiki/Behavior_driven_development) framework
 * [Flexmock](http://flexmock.rubyforge.org/) and/or [Mocha](http://mocha.rubyforge.org/) - for mocking/stubbing

### Misc
 * [tzinfo](http://tzinfo.rubyforge.org/) - Time Zone handling
