---
author: brandon.dimcheff
date: 2007-09-10T00:00:00Z
title: Caboose Sample App
url: /2007/09/10/caboose-sample-app/
tags: []
---

The [Caboose sample app](http://sample.caboo.se/) comes with user authentication and many other useful features baked in.  It's a great starting place for your new Rails app!
