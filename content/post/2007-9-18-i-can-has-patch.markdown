---
author: brandon.dimcheff
date: 2007-09-18T00:00:00Z
title: I can has patch?
url: /2007/09/18/i-can-has-patch/
tags: []
---

I was poking through svn for [has\_many\_polymorphs](http://blog.evanweaver.com/files/doc/fauna/has_many_polymorphs/files/README.html) to see if a bug that I found had been fixed.  Lo and behold, it was:

> r688 | evanweaver | 2007-08-29 03:49:55 -0400 (Wed, 29 Aug 2007) | 2 lines
>
> fix singularize bug in autoloader (Brandon Dimcheff)
