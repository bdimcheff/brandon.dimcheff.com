---
author: brandon.dimcheff
date: 2007-09-07T00:00:00Z
title: Massive list of Rails development tips from Peepcode
url: /2007/09/07/massive-list-of-rails-development-tips-from-peepcode/
tags: []
---

The [Peepcode](http://peepcode) people have published a long list of Rails development tips: [Tips!](http://nubyonrails.com/articles/massive-list-of-rails-development-tips)
