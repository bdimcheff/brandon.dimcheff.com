---
author: brandon.dimcheff
date: 2007-09-07T00:00:00Z
title: Railsify
url: /2007/09/07/railsify/
tags: []
---

A new Rails plugin repository has been launched: [Railsify](http://railsify.com/).  Check it out.
