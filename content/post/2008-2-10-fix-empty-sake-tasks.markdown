---
author: brandon.dimcheff
date: 2008-02-10T00:00:00Z
title: Fix empty sake tasks
url: /2008/02/10/fix-empty-sake-tasks/
tags: []
---

I've had a problem with my [sake](http://errtheblog.com/posts/60-sake-bomb) tasks being blank for the past couple days.  Since there were a couple comments on err about similar things, I'll share my fix: `gem install -v 2.0.1 ParseTree`.  That's it.  Turns out my ParseTree install was messed up and to\_ruby was not working on the task blocks.
