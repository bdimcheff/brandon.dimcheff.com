---
author: brandon.dimcheff
date: 2008-06-18T00:00:00Z
title: Make RubyMate work with MacPorts Ruby
url: /2008/06/18/make-rubymate-work-with-macports-ruby/
tags: []
---

To make TextMate's RubyMate work with MacPorts (or any other non-default ruby install) just set TM\_RUBY environment variable to be the path to your custom ruby interpreter in TextMate's preferences.  Custom environmental variable settings are located under 'Advanced'->'Shell Variables' in TM's prefs.
