---
author: brandon.dimcheff
date: 2008-08-22T00:00:00Z
title: Ruby Hoedown Videos Online
url: /2008/08/22/ruby-hoedown-videos-online/
tags: []
---

Ruby Hoedown 2008 [videos](http://rubyhoedown2008.confreaks.com/) are now live.  There are a lot of good talks.  I highly recommend checking them out.  I've set up a [torrent](http://thepiratebay.org/torrent/4355220) of all the videos for your convenience.
