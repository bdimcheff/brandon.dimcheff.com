---
date: 2009-03-04T00:00:00Z
title: Fix Missing Disqus Comment Form
url: /2009/03/04/fix-missing-disqus/
tags: []
---

I had a bit of trouble with Disqus where it would only display a link saying "Discuss this topic on Disqus" instead of displaying an actual comment form.  It turns out that this problem was caused by a mismatch between the URL configured in Disqus and the url where my blog was actually accessible.  So if you're missing a Disqus comment form, make sure that the URL listed under Settings -> Website URL in Disqus is the actual URL where your site resides.
