---
date: 2009-03-17T00:00:00Z
title: Automatically log in on merb-auth account creation
url: /2009/03/17/automatically-log-in-on-merb-auth-account-creation/
tags: []
---

Recently I was adding new user registration to [tasteb.in](http://tasteb.in) and I wanted my users to be automatically logged in as soon as they created their account.  I'm using merb for tasteb.in, and consequently merb-auth for authentication.  It turns out it's really easy to implement, but it took me a while to get it right.  All you need to do is add the following to your action that creates the new user:

{{< highlight ruby >}}session.user = @user{{< / highlight >}}

That's it.  All you must do is set the user in the session to the newly created user.  I hope this saves somebody else some time.
