---
author: brandon.dimcheff
date: 2009-01-02T00:00:00Z
title: Resolutions
url: /2009/01/02/resolutions/
tags: []
---

I generally think that new year resolutions are silly, but since it's the new year and I have some goals I'd like to set, I guess I might as well call them resolutions.

Since goals, like technical requirements, are meaningless if you can't measure them, I'll be as specific as possible even if it means that I'll have to adjust them later.  So here we go:

Resolved:

* I will blog at least once a week. Corollary: I will blog within one day of saying to myself  "Hey, I should blog about this"
* I will contribute a patch to an open source project at least once a month
* I will start a useful open source project by April
* I will write my blog from scratch by July so I can do it just the way I want
* I will do [Project 365](http://photojojo.com/content/tutorials/project-365-take-a-photo-a-day/).  Seriously this time.

and perhaps most importantly:

* I will have my next form of gainful employment arranged by January 23rd.

This blog post does not count towards resolution #1, and I've got a decent backlog of items to write about, so you'll be hearing from me soon.

Happy new year!
