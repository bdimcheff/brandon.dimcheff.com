---
author: brandon.dimcheff
date: 2009-01-26T00:00:00Z
title: Run specs with autotest, er, autospec!
url: /2009/01/26/run-specs-with-autotest-er-autospec/
tags: []
---

It seems that it's not well publicized, but if you want to run your RSpec files with `autotest` try `autospec` instead.  It looks inside the spec/ directory for files named "\*\_spec.rb" by default for specs.  `autospec` is part of the RSpec gem, and basically just sets up the proper environment and then shells out to autotest.

Also, if your files in lib/ match up with specs in spec/, `autospec` will automatically re-run your specs for changed files in lib/ on save.  So if you have lib/foo/bar.rb, `autospec` will re-run lib/foo/bar\_spec.rb when you save bar.rb.
