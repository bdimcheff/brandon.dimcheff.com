---
author: brandon.dimcheff
date: 2009-01-03T00:00:00Z
title: Use Capistrano to set your production database password
url: /2009/01/03/use-capistrano-to-set-your-production-database-password/
tags: []
---

This is a Capistrano recipe I use to configure my production database.yml.  Before `deploy:setup` executes, this recipe will prompt for a database password and save a new database.yml file to the shared directory on your server.  After each deploy, the database.yml file will be symlinked from the shared directory.  You can reconfigure your database by executing `cap db:configure`.

Here are the goods.  I'm pretty sure I got this from [Jeremy Voorhis](http://archive.jvoorhis.com/articles/2006/07/07/managing-database-yml-with-capistrano) a while back, and modified it to work with Capistrano 2.

<script src="http://gist.github.com/42755.js"></script>
