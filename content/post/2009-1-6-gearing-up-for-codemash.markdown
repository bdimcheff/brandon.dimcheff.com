---
author: brandon.dimcheff
date: 2009-01-06T00:00:00Z
title: Gearing up for CodeMash
url: /2009/01/06/gearing-up-for-codemash/
tags: []
---

I'm getting all packed for CodeMash!  Hope too see a lot of you there.  I'm a little bit disappointed by the dearth of Ruby sessions, but there are enough of us that the open sessions will be rockin'.  And the ever popular scotch track always helps.

[![CodeMash](http://codemash.org/images/badges/attendee1.png)](http://codemash.org)
