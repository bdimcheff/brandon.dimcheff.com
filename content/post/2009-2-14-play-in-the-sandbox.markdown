---
author: brandon.dimcheff
date: 2009-02-14T00:00:00Z
title: Play in the Sandbox
url: /2009/02/14/play-in-the-sandbox/
tags: []
---

I needed to test some code that reads and writes files on the filesystem.  I got sick of manually setting up a scratch area for my test files and cleaning them up when I was done, so I created [Sandbox](http://github.com/bdimcheff/sandbox/tree/master) to keep my tests DRY.

See the Sandbox readme for more information, but the basic idea is this:

{{< highlight ruby >}}# inside a test somewhere

Sandbox.play do |path|
  file = File.join(path, 'foo')
  FileUtils.touch(file)  # build a sand castle

  assert File.exists?(file)  # test whatever you want in here
end

assert !File.exists?(file)  # path has been deleted
{{< / highlight >}}

The project is currently only on [github](http://github.com/bdimcheff/sandbox/tree/master), but it will be on RubyForge once approved.  If you want to use it now, you have to use the [github gems source](http://gems.github.com/) or install it from source.

## Future plans

* Optionally `Dir.chdir` to the temporary directory for even easier usage
* Supply a template directory/archive to be used for the base state of the sandbox
