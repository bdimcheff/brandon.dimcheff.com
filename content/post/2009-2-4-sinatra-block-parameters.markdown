---
author: brandon.dimcheff
date: 2009-02-04T00:00:00Z
title: Sinatra block parameters!
url: /2009/02/04/sinatra-block-parameters/
tags: []
---

The latest master [Sinatra](http://www.sinatrarb.com/) now supports optional block parameters.  It captures any parameters in the URL and passes them into the block that defines the action:

{{< highlight ruby >}}
get '/hello/:name' do |n|
  "Hello #{n}!"
end
{{< / highlight >}}


The `params` hash is still available as before, so this should not break any existing applications.  It will also work with regular expressions.  Any captures are yielded in order:

{{< highlight ruby >}}
get %r{/hello/([\w]+)/([\w]+)} do |a, b|
  "Hello, #{a} and #{b}!"
end
{{< / highlight >}}

Big thanks to [rtomayko](http://tomayko.com) for accepting my patch, and working with me to get this solid enough to put into Sinatra!

One final note: this feature behaves slightly differently in ruby 1.8 and 1.9.  Ruby 1.9 is much stricter about block arity than 1.8 is.  If the number of captures in the URL and the arity of the block do not match, 1.9 will raise an exception.  1.8 will likely just ignore the extra parameters and throw up a warning.  If you write code that is compatible with 1.9, it will also work with 1.8, as ruby 1.9 is simply more strict.
