---
author: brandon.dimcheff
date: 2009-02-05T00:00:00Z
title: How to learn Ruby
url: /2009/02/05/how-to-learn-ruby/
tags: []
---

**UPDATE 4/20/2010:** Well Grounded Rubyist is not prerelease anymore.  Corrected.

**UPDATE 6/30/2011:** Fix links to \_why's poignant guide, since he vanished from the earth. Add a bit about finding live people to help you.

**UPDATE 9/27/2015:** Add udemy ruby guides. More updates coming soon, as many books have been updated since I originally posted this.

Several people have asked me for tutorials, books, and other resources to help them learn Ruby, so I've attempted to compile a list of resources I feel are particularly valuable for beginning Rubyists.  None of these resources are Rails-specific, but being a good Ruby programmer will obviously help you use Rails more effectively.  If there is interest, I will compile a similar (and likely longer) list of Rails resources.

Also, if I've missed anything that helped you learn or teach Ruby, please leave a comment and I will add it to the page.  I am especially interested in resources that beginning Rubyists have found useful.

For these resources, I assume you already have programming experience and are attempting to pick up Ruby to widen your skillset.  If you're new to programming entirely, check out Chris Pine's [Learn to Program](http://pine.fm/LearnToProgram/) online tutorial.

## The 100% Best Way to Learn Ruby
The best way (in my not so humble opinion) to learn Ruby is to find a Ruby expert who is willing to mentor you.  Maybe this means tagging along with somebody at work, or pairing with somebody at a [startup weekend](http://startupweekend.org/).  At our Ruby meetups in Ann Arbor, we occasionally do a Ruby tutorial where people take turns working on a simple problem that is projected up on a board.  New rubyists have often found a permanent guide at these sorts of events.  The value of having somebody close to you to be able to help is profound.  Do this if you can.  If you can't, or for when you're alone, this post has many other non-sentient resources that you can use.

## Online Resources
The following is a list of freely available online resources that I've found particularly helpful for new Rubyists.

**[Ruby Koans](http://github.com/edgecase/ruby_koans/tree/master):** The newest kid on the block (no pun intended) that's taking github by storm is EdgeCase's [Ruby Koans](http://github.com/edgecase/ruby_koans/tree/master).  The [Koans](http://en.wikipedia.org/wiki/Koan) are essentially a series of failing test cases that you need to figure out how to fix in order to move on to the next step.  The best way to learn anything is by doing, and this tutorial will get you programming in no time flat.  We use this one at our "ruby for noobies" meetings a lot.

Note that the point in the exercise is **not** to fix all the tests as fast as possible.  In order to get the benefit of this exercise, you must read and understand what the code is showing you.  Complete each Koan one by one.  Run your tests after each time you change your code.

I think the best way to use these Koans is to get in a small (2-3 person) group and take turns solving the problems.  Discuss what you are doing with each other.  Don't take anything for granted.

**[Why's Poignant Guide](http://mislav.uniqpath.com/poignant-guide/):** Why the Lucky Stiff put together an extremely silly tutorial on Ruby.  While it may seem a bit campy, it really is a good tutorial.  And it's a lot of fun!  It's not nearly as exhaustive as the Koans, but it has more cartoon foxes and chunky bacon.

**[Ruby Quiz](http://rubyquiz.com/):** Once you get your feet wet, you might want to check out the Ruby Quiz archives.  There are over 150 small programming challenges that you can try to solve using Ruby.  Don't cheat and look at the answers before you try it for yourself!

**[10 Things Every Java Programmer Should Know About Ruby](http://onestepback.org/articles/10things/):** If you're a Java programmer trying to learn ruby, you should check out Jim Weirich's 10 things presentation.  He goes over many important differences between Java and Ruby, and this can really help you avoid mistakes that many Java programmers make when transitioning to Ruby.

**[The Humble Little Ruby Book](http://humblelittlerubybook.com/):** Jeremy McAnally's free introductory online Ruby book.  Covers the basics about a lot of stuff that every Ruby programmer needs to know.  Also available in print.

**[Udemy's Intro Ruby on Rails Guide](https://blog.udemy.com/ruby-on-rails-tutorial-learn-from-scratch/):** [Udemy](http://udemy.com), one of the new platforms for online learning, has an intro to Rails in step-by-step tutorial form. If you're interested in Rails specifically, it's definitely worth checking out.

## Books
The following books are, in my opinion, the best books for aspiring Rubists to check out.  None of them are Rails specific.

**[Beginning Ruby: From Novice to Professional](http://www.amazon.com/gp/product/1430223634/ref=as_li_ss_tl?ie=UTF8&tag=itsaruth-20&linkCode=as2&camp=217145&creative=399369&creativeASIN=1430223634):** [Peter Cooper](http://www.petercooper.co.uk/)'s Beginning Ruby book has received rave reviews, and is specifically targeted at beginning Rubyists.  I'm a bit ashamed that I didn't pick it up on the first pass at this article.  Sorry Peter!

**[Programming Ruby: The Pragmatic Programmers' Guide, Second Edition](http://www.amazon.com/gp/product/0974514055?ie=UTF8&tag=itsaruth-20&linkCode=as2&camp=1789&creative=9325&creativeASIN=0974514055) (aka. The Pickaxe):**
This book has been the gold standard of Ruby books for years, and is a must have for ruby programmers.  It has both a descriptive section and a reference section.  The 2nd edition is from 2004, however, and does not cover Ruby 1.9 at all.  The 3rd edition is available in [beta](http://www.pragprog.com/titles/ruby3/programming-ruby-1-9), and the final version will be available in March.

**[The Ruby Way](http://www.amazon.com/gp/product/0672328844?ie=UTF8&tag=itsaruth-20&link_code=as3&camp=211189&creative=373489&creativeASIN=0672328844):** Hal Fulton's book is full of solutions to common problems, plus it really helps you learn many of the Ruby idioms that are used by experienced Ruby programmers.  Ruby 1.8 only.

**[The Ruby Programming Language](http://www.amazon.com/gp/product/0596516177?ie=UTF8&tag=itsaruth-20&linkCode=as2&camp=1789&creative=9325&creativeASIN=0596516177):**
Written by the creator of Ruby himself, this book covers ruby 1.8 and 1.9.  It's not a tutorial-style book, nor a complete reference, but more of description of the important parts of the Ruby programming language.  It's a great second book (after the Pickaxe) to help you understand more details about how Ruby works.

**[The Well Grounded Rubyist](http://www.amazon.com/gp/product/1933988657/ref=as_li_ss_tl?ie=UTF8&tag=itsaruth-20&linkCode=as2&camp=217145&creative=399369&creativeASIN=1933988657):**
David Black's newest book *The Well Grounded Rubyist* is now available in its final edition.  It's in tutorial form, which is generally good for beginners.  I have not had a chance to read it yet, but his last book was great and I hear this one's even better.

**Note:**  This is a work in progress.  I will be updating it as new books and tutorials come out.  I also don't necessarily want an exhaustive list of every ruby resource.  I'm looking for resources that are helpful for beginners.  Too many choices may be overwhelming.  Also, I haven't been a Ruby beginner in a long time, so I am particularly interested in things that recent beginners have found helpful.
