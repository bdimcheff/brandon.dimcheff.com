---
author: brandon.dimcheff
date: 2009-03-01T00:00:00Z
title: tasteb.in is up!
url: /2009/03/01/tasteb-in-is-up/
tags: []
---

I've been working on a Merb-based recipe paste site for the past couple weeks called [tasteb.in](http://tasteb.in).  It's extremely simple for now.  It lets you paste recipes in a very simple format and they show up on the site.  You can access your recipes and all recipes on the site.  And that's about it.

I'll be going into more details about how tasteb.in is built and where I'd like to take the site, but for now, just play with it if you like.  Drop me a line with any problems you have, or features you would like to see.
