---
author: brandon.dimcheff
date: 2010-04-17T00:00:00Z
title: Ruby Brew at Great Lakes Ruby Bash
url: /2010/04/17/ruby-brew-at-great-lakes-ruby-bash/
tags: []
---

<span style="margin:0 10px; float:right"><img src="/images/ruby_brew.jpg" width="240" height="160" alt="ruby brew" /></span>
John Roos of [Roos Roast](http://roosroast.com) designed an excellent blend of coffee for Great Lakes Ruby Bash.  John started Roos Roast while selling cars at Dunning Subaru.  I met him there when I bought my WRX, when he gave me a free pound of coffee with my car.  Not long after that, he quit selling cars and started roasting coffee full-time.

We have enough coffee to make 300 cups during the conference, plus we have 6 half-pound bags of coffee to raffle off for the attendees.

Huge thanks to John for doing this for us.  If there's enough demand, he might be convinced to roast more batches.  Call him up or stop by his [roasting operation](http://roosroast.com/about/) and demand some Ruby Brew!

If you're not in the Ann Arbor area, you can order his coffee by phone or [online](http://roosroast.com/shop/ruby-brew/).

Working on this project with John was a lot of fun.  I watched as he carved the Ruby Brew logo into a rubber stamp block.  He also put me to work a bit.  I stamped 300 of our cups, and weighed and ground our coffee for the conference.

<object width="400" height="300"> <param name="flashvars" value="offsite=true&lang=en-us&page_show_url=%2Fphotos%2Fmoofbong%2Fsets%2F72157623843059602%2Fshow%2F&page_show_back_url=%2Fphotos%2Fmoofbong%2Fsets%2F72157623843059602%2F&set_id=72157623843059602&jump_to="></param> <param name="movie" value="http://www.flickr.com/apps/slideshow/show.swf?v=71649"></param> <param name="allowFullScreen" value="true"></param><embed type="application/x-shockwave-flash" src="http://www.flickr.com/apps/slideshow/show.swf?v=71649" allowFullScreen="true" flashvars="offsite=true&lang=en-us&page_show_url=%2Fphotos%2Fmoofbong%2Fsets%2F72157623843059602%2Fshow%2F&page_show_back_url=%2Fphotos%2Fmoofbong%2Fsets%2F72157623843059602%2F&set_id=72157623843059602&jump_to=" width="400" height="300"></embed></object>

Thanks, John!
