---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "On the AAPD Contract Renewal"
subtitle: "and defunding the police"
summary: "a statement I intended to give at Ann Arbor City Council re: police contract negotiations and funding"
authors: []
tags: ["a2council", "police"]
categories: []
date: 2020-07-06T18:07:01-04:00
lastmod: 2020-07-06T18:07:01-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

I originally wrote this to read aloud at city council, but it looks like my slot disappeared. Instead I sent it via email, even though it's not really designed for written form.

--------

Hi, my name is Brandon Dimcheff, and I live in the 4th ward. I want to talk about the collective bargaining agreement with the AAPD, and policing in Ann Arbor.

It's easy to assume that we're special, and thus immune to the systemic injustices built into American policing. Our police department is probably among the best by conventional metrics, and we live in a generally peaceful community. But we are very much not exempt from the problems of how policing and criminal justice are administered, and we should not act like we're 30 square miles surrounded by reality, as people are fond of saying.

For every Aura Rosser, there are hundreds? thousands? of people who are threatened with violence unnecessarily but are luckily still alive. People with addiction problems that end up in the criminal justice system instead of being treated for their addiction. People in jail for minor offenses, catching a potentially fatal disease. In cases like these, no amount of oversight will result in justice, as the problems are not caused by individual officers, but by the system itself. We must stop using violence where it is unnecessary and counterproductive. This is Ann Arbor. We can do better.

Oversight (and ICPOC) are necessary but not sufficient. Accountability for officers is necessary but not sufficient. Transparency and availability of policing data is necessary but not sufficient. Contract changes may also be necessary, but are not sufficient.

To that end, I agree that the ICPOC-suggested changes to the contract are worthwhile, but even if they are approved, those changes must be only the beginning. Spend the political capital to change policing and law enforcement to truly protect our fellow citizens. Take easy wins where you can. Prioritize changes that will actually make a difference. Don't think that changing the contract and empowering ICPOC is enough to offset hundreds of years of systemic racism.

Consider, for instance:

1. providing social workers that can take the place of police interactions in situations that don't require an armed response
1. funding mental health and addiction treatment services so we can address the root causes of a lot of crime
1. finding alternative enforcement options for civil infractions
1. getting rid of laws that provoke unnecessary interactions with the police, particularly those that are disproportionately used on people of color
1. and most importantly: listening to experts who have been working in this space for years, including those on ICPOC and local advocates

How should we pay for it, you may ask? Well we spend $30M (or approximately 1/3 of our budget) on AAPD every year. Reallocate some of it to address the root causes of crime instead. Public safety will benefit overall, and we will make Ann Arbor a more just and equitable place.

Thank you for your time.

\- brandon
