---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Covid and Dining"
subtitle: ""
summary: "Where I argue that we should in-person dining closed until people are vaccinated, particularly indoor dining."
authors: []
tags: ["a2council", "politics", "covid"]
categories: []
date: 2021-02-21T10:59:37-05:00
lastmod: 2021-02-21T10:59:37-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
At the City Council meeting on February 16, 2021, Ann Arbor City Council approved a [resolution](http://a2gov.legistar.com/LegislationDetail.aspx?ID=4774001&GUID=F4FFC1E6-2E37-438C-8E7F-316F1DD95424&Options=&Search=&FullText=1) allowing Conor O'Neill's to use Main St. to provide outdoor seating for St. Patrick's Day. It provoked a pretty immediate negative reaction from a lot of people around town, and the mayor ended up vetoing it. I personally think that indoor dining is a much larger problem than this would have been, but we shouldn't be doing any of this stuff, particularly now.

If you're trying to get a covid vaccine in Michigan right now, check out [VaccineMI](https://www.vaccinemi.com/), where we're trying to compile Michigan vaccine availability all in one place.

Here's what I sent to City Council after the veto:

---------

Thanks for reconsidering restarting outdoor dining for St. Patrick's day. While the initial visceral reactions were stronger than the severity of the risk introduced by this particular event, I think it was the right call to cancel it. Outdoor transmission has occurred (remember the Rose Garden?) it's just less likely than indoors. Even if the event itself is relatively safe, the loosening of restrictions anywhere may cause people to take fewer precautions in other areas of life. "Oh people are eating outdoors on Main St., we might as well have a barbecue", etc.

I know everyone is suffering from pandemic fatigue, but now is a particularly terrible time to increase spread. Early in the pandemic, when it was unclear if or when there would be a vaccine, it made sense to flatten the curve to avoid overwhelming medical facilities, even if the same number of people would inevitably become sick eventually, particularly when it was obvious we were not going to control it at a national level. Now that we have effective vaccines, the benefit of slowing the spread is more acute. We're no longer simply delaying people's illnesses and deaths, we're preventing them entirely. Every case we can delay until after someone is vaccinated is (effectively) a case avoided. For every ~100 cases we avoid, we save a life. We should be doing everything possible to delay cases until we can get enough shots into arms.

We've been at this for nearly a year now. In a few more months, if all goes well with the vaccine rollout, we'll be able to get back to a much more normal life. The end is in sight. As they say after you land on a plane: "please remain seated with your seatbelt fastened until the aircraft is at the gate and the captain turns off the seatbelt sign." Get up too early and you're likely to end up with a bloody nose.

Please consider future policy changes in this context. We have an opportunity to save a ton of lives, and we should take it. I know the support from the State and Federal governments hasn't been great, and we can't singlehandedly implement a zero-covid policy like Australia or New Zealand, but we should do what's in our power at the local level to save as many lives as possible over the next few months. I understand that we can't control everything, and some things move risk from one place to another, but minimizing overall risk is the most important thing we can do. Honor the 250 lives we've lost in Washtenaw County by preventing more unnecessary deaths.

One thing is abundantly clear to me: we should not be doing indoor dining right now. There is no virtue to indoor dining, it is simply high-risk fun. If we can _replace_ indoor dining with outdoors, that is a net risk reduction, but _supplementing_ indoor dining with outdoor dining increases overall risk. Carryout is nearly zero-risk. I know restaurants are hurting but let's convince everyone to get carryout, tip well, and eat at home, just for a few more months. Encourage people to take a masked walk with friends around gallup if they want in-person human contact for at least a 20x less risky in-person activity.

For more on this topic, I highly recommend this the Osterholm Update podcast generally, and [this week's episode](https://www.cidrap.umn.edu/covid-19/podcasts-webinars/episode-44) is particularly relevant. I think it's a huge mistake to allow nonessential, in person, unmasked activities just as cases are finally falling and we have a new variant taking off. Just like Dr. Osterholm, I hope I'm wrong, but I think things are about to get a lot worse before they finally get better as we get more people vaccinated.

I've found [Microcovid](https://www.microcovid.org/) to be very useful, and it has guided me in composing this letter: it has a risk calculator that allows you to input various scenarios to see how risky they are. Their math is aiming to keep your likelihood of catching covid under 1% per year, and allows you to see how much each activity "spends" your covid budget. It's obviously not perfect, but it gives you a good idea of relative risks. It's adjusted for county numbers periodically, so the numbers below are what applies on February 21, 2021 and may change if you visit these links later. For example:

* [Outdoor dining for 90min](https://www.microcovid.org/?distance=sixFt&duration=90&interaction=oneTime&personCount=15&riskProfile=average&setting=outdoor&subLocation=US_26161&theirMask=none&topLocation=US_26&voice=normal&yourMask=none) in Washtenaw County is 100% of your weekly covid budget.
* [A 6h plane ride with middle seats empty](https://www.microcovid.org/?distance=sixFt&duration=360&interaction=oneTime&personCount=20&riskProfile=average&setting=plane&subLocation=US_26161&theirMask=basic&topLocation=US_26&voice=silent&yourMask=basic) is only 80% of your weekly budget.
* [Going to the grocery store for an hour](https://www.microcovid.org/?distance=sixFt&duration=60&interaction=oneTime&personCount=10&riskProfile=average&setting=indoor&subLocation=US_26161&theirMask=basic&topLocation=US_26&voice=silent&yourMask=basic) is about 40% as risky, if properly masked.
* 🚨 [Indoor dining with **only** 6 people within 15 feet of you](https://www.microcovid.org/?distance=sixFt&duration=90&interaction=oneTime&personCount=6&riskProfile=average&setting=indoor&subLocation=US_26161&theirMask=none&topLocation=US_26&voice=normal&yourMask=none) is 8x your weekly covid budget. 🚨
* [Going for a masked walk in Gallup with a couple of friends](https://www.microcovid.org/?distance=sixFt&duration=60&interaction=oneTime&personCount=2&riskProfile=average&setting=outdoor&subLocation=US_26161&theirMask=basic&topLocation=US_26&voice=normal&yourMask=basic) is nearly risk-free.
* [Picking up carryout afterwards](https://www.microcovid.org/?distance=sixFt&duration=5&interaction=oneTime&personCount=2&riskProfile=average&setting=indoor&subLocation=US_26161&theirMask=surgical&topLocation=US_26&voice=normal&yourMask=surgical) is also nearly risk-free, and with curbside pickup it effectively is zero-risk.

Stay healthy,

Brandon Dimcheff, Ward 4
