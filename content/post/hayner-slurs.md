---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Consequences"
subtitle: ""
summary: "Councilmember Hayner used a homophobic slur to attack the local press, and I think he should face consequences as a result."
authors: []
tags: ["a2council", "politics"]
categories: []
date: 2021-04-18T16:42:53-04:00
lastmod: 2021-04-18T16:42:53-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
I sent the email below to City Council after I became frustrated that what seemed like a broad condemnation of hateful, inappropriate speech turned into a [factional fight](https://twitter.com/AaKarenin/status/1382819266882322441) instead of a unanimous rebuke of the poor behavior that plagues our local politics here in Ann Arbor.

First, some background for those new to this particular area, and a **content warning**: my letter does not contain slurs, but some of the articles and tweets I have linked contain homophobic slurs.

The best long-form summary of what's going on is probably this [Michigan Daily article](https://www.michigandaily.com/ann-arbor/ann-arbor-councilmember-jeff-hayner-defends-posting-homophobic-slur-after-criticism-from-fellow-councilmember/), but tl;dr Hayner posted a quote from Hunter S. Thompson's _Fear and Loathing in Las Vegas_ that contains a homophobic slur, ostensibly to attack the local press for coverage of him that he didn't like. When people were upset by his actions, he dug in his heels and refused to apologize for a while, and then finally issued a short pseudo-apology with no real details or commitment to do better.

CM Hayner has a history of being an edgelord on [twitter](http://information.sensoryresearch.net/hayner/) that predates his term on council, which [The Daily](https://www.michigandaily.com/ann-arbor/new-councilmember-disparaged-now-colleagues-tweets-archive-shows/) has also covered. This is a pattern of bad behavior, and it has gone on for too long. It is time to stop tolerating it.

--------

Greetings, Ann Arbor City Council.

I would like to briefly address CM Hayner's offensive public comments and DC-5. Well, actually I don't enjoy this at all. I would prefer to use this email to discuss my excitement about our efforts around housing and non-motorized infrastructure coming to fruition in CA-2 through 5, C-1, and C-2, but unfortunately CM Hayner's behavior has forced me to address council on this unpleasant topic.

From what I can tell, every Councilmember agrees that the statements that CM Hayner made were inappropriate, particularly for someone holding elected office in Ann Arbor. The question, then, is what is the appropriate response from council?

If this was CM Hayner's first time exhibiting such behavior, I'd be inclined to consider a public apology a reasonable response. Unfortunately, CM Hayner has shown an escalating pattern of hostility towards other members of council, the press, and members of the community who disagree with him. When confronted with criticism of these attacks, he has repeatedly refused to accept responsibility for his actions.

In this particular case, he defended his post, has lied about how long it was up, said that people just "want to be offended". His defense amounted to "I'm not attacking the LGBTQ community, I'm attacking the press," which is also not a reasonable position for a public official to take. After significant community pressure, he has released a very short apology, which many of his constituents and community members cannot view because he has blocked them on social media. His apology does not convince me that he understands the ways his actions were harmful, nor does it convince me that he has committed to repairing the damage and ensuring that something similar won't happen again.

Again, this is far from the first time that CM Hayner has exhibited this sort of behavior. He refused to accept responsibility for his inflammatory and misogynistic tweets from before he was elected and instead has continued inflammatory posts on social media. Instead he has attacked those who have pointed them out. He has refused to accept responsibility for his troubles with the building department, instead choosing to attack those who discovered his unpermitted addition and vaguely threatening people who approach his house.

"To have better behavior, we simply need to behave better. It should not be that hard, and it should not require more rules," said CM Hayner a couple months ago. He clearly hasn't taken his own advice, so it's up to Council to take action. Please listen to advocates, particularly those from communities that CM Hayner has harmed, and act. Council clearly has the authority to implement the actions in DC-5, as council has broad discretion to remove committee members for cause. Please use that authority to show the community that this behavior will not be tolerated.

I know there have been some concerns around the First Amendment, but if that's the case then Congress and the Board of Regents would also have been in violation for removing Marjorie Taylor Greene and Ron Weiser from their committee assignments. Nobody has seriously suggested that those actions violate the Constitution. You have the authority to act, and Ann Arborites will not accept excuses about lack of authority as anything other than an attempt to defend this use of bigoted language.

I'd like to particularly address CMs who may be hesitant to act here due to political alliances: please put City over Party. The members of Congress who were recently willing to cross party lines and do the right thing will be remembered for their courage, while those who didn't were rightly criticized for having misaligned priorities. Sending a message that Ann Arbor rebukes intolerance will be more powerful if conveyed unanimously. That is what our community demands and deserves.
