+++
title = "hello again"
date = 2017-10-16T20:32:12-04:00
draft = false

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["hugo"]
categories = []

abstract = "Well, it's been more than 7 years since I've blogged, but I've collected a pretty large list of topics."

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++

Well, it's been more than 7 years since I've blogged, but I've collected a pretty large list of topics.  But first, some people and and projects deserve credit for my fancy new site.  Stay tuned for actual content.

## Hugo

I've been doing a fair amount of work with go lately, primarily due to increasing use of Kubernetes.  Since [Hugo](https://gohugo.io/) is written in go and we're using it for our website at [Olark](https://www.olark.com), I thought it would be a good excuse to check it out.

## Academic Theme

Since I'm a bad designer, I needed to use an off-the-shelf theme.  Hugo has a lot of [themes](https://themes.gohugo.io/) available, and I settled on the [Academic](https://github.com/gcushen/hugo-academic) theme.  It's a nice combination of a single-page homepage with a bio and sections for portfolio items, but also includes separate pages for each subsection.

## GCS Deploy

I stole my GCS deploy from [this](https://blog.bramp.net/post/2015/08/01/hugo-makefile/) blog post, and added `gsutil rsync` functionality.

```makefile
.PHONY: all clean watch help deploy

HUGO := hugo
GSUTIL := gsutil
WEBSITE_URL := brandon.dimcheff.com

# All input files
FILES=$(shell find content layouts static themes -type f)

# Below are PHONY targets
all: public

help:
	@echo "Usage: make <command>"
	@echo "  all     Builds the blog and minifies it"
	@echo "  clean   Cleans all build files"
	@echo "  watch   Runs hugo in watch mode, waiting for changes"
	@echo ""
	@echo "New article:"
	@echo "  hugo [-k kind] new post/the_title"
	@echo "  $$EDITOR content/post/the_title.md"
	@echo "  make watch"
	@echo "  open "

clean:
	-rm -rf public

watch: clean
	$(HUGO) server -w

# Below are file based targets
public: $(FILES) config.toml
	$(HUGO)

	# process files here if necessary

	# Ensure the public folder has it's mtime updated.
	touch $@

deploy: public
	$(GSUTIL) -m rsync -R public gs://$(WEBSITE_URL)
```
