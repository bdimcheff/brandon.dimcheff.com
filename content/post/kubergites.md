+++
title = "Kubergites"
date = 2017-10-29T23:49:53-04:00
draft = true

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = []
categories = []

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++

Kubernetes is excellent at keeping a running system as close to a declared state as possible.  If I ask it to create 4 pods of a particular service, it will happily do so or tell me why it can't.

My dream is to be able to use a heterogenous set of tools to create objects in Kubernetes, view a diff of the running system before applying a change, and be able to transactionally roll back entire changesets if something goes wrong.  A reasonable analogy for this would be comparing a normal filesystem to git: If you overwrite a file on your hard drive, you better have a backup, but if you overwrite a file in git, a quick `git checkout` and you're back to your previous state.

At Olark we currently have a git repository that contains helm charts that represent much of our infrastructure.  When we update a chart, our CI system will update the running system with our changes.  If there was an error, we can roll back _that chart_ to a previous version using helm or a CI task.  


```
$ kubectl changeset create foo
$ kubectl apply -f <whatever>
$ kubectl changeset diff
<show changes between changeset and currently running system>
$ kubectl changeset apply
$ kubectl changeset rollback
$ kubectl changeset log
$ kubectl changeset dump > prod.yml
# switch to staging
$ kubectl changeset load < prod.yml
```