---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Letter to the Michigan Board of State Canvassers"
subtitle: "wherein I tell them to respect election results in a democracy"
summary: "This is a letter I wrote to the canvassers to encourage them to accept the results of the 2020 presidential election, since it seems that may not automatically happen."
authors: []
tags: ["politics"]
categories: []
date: 2020-11-22T13:53:34-05:00
lastmod: 2020-11-22T13:53:34-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

## Background

There is a [meeting](https://www.michigan.gov/documents/sos/11232020_Mtg_Notice__Materials_708509_7.pdf) of the Michigan Board of State Canvassers on Monday, November 23, 2020 at 1pm where they are duty-bound to certify the election results from the November 3rd presidential election in Michigan. After the Wayne County board briefly failed to certify the county results, there is some concern that the State board will do the same. This is an unwelcome attack on a fair election, and an attempt to disenfranchise Michiganders.

There are public comment opportunities, both written and via zoom, that you can participate in via filling out [this surveymonkey](https://www.surveymonkey.com/r/ZMBNHVC) form.

For more details on what happened in Wayne County, check out [@beyond_process](https://twitter.com/beyond_process/status/1328841565834522624)'s thread and this [NYT article](https://www.nytimes.com/2020/11/17/us/politics/michigan-certify-election-results.html). You may have seen Ned Staebler's [viral commentary](https://twitter.com/beyond_process/status/1328841565834522624) on MSNBC, The Late Show, The Daily Show, etc.

I chose to write a letter, since I'm no Ned, which is below. If you're a Michigander and have a sec to write a letter, or have time on Monday to be on a zoom for hours, I encourage you to do so.

## Letter

Michigan Board of State Canvassers,

I'm writing to ask that you please certify the election results without delay and political posturing. There are no signs of irregularity besides a handful of clerical errors that occur at every election and are inconsequential to the final result. Risk-limiting audits have been performed by the Secretary of State and they showed no irregularities.

The voters of Michigan have spoken, and attempting to undermine the election results is unwelcome and unamerican. These attempts will almost certainly fail, but the delays and uncertainty are going to cost American lives and erode our national security. Eroding faith in the results of a fair election is the stuff of tin-pot dictators, not a democratic republic that obeys the rule of law.

Put country over party, and over the demands of one man who is afraid to accept reality. It is your patriotic and legal duty to certify the results. History will remember what you do today.

\- Brandon Dimcheff