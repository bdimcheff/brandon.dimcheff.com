---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "The Future of Ann Arbor"
subtitle: "2020 City Council Elections"
summary: "Let's solve our housing crisis. Vote for Lisa Disch, Linh Song, Travis Radina, Jen Eyer, or Erica Briggs."
authors: []
tags: ["a2council", "politics"]
categories: []
date: 2020-08-02T15:53:22-04:00
lastmod: 2020-08-02T15:53:22-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

We have a problem in Ann Arbor: more people want to live here than there is available housing. When housing demand exceeds supply, prices go up and as a result only rich people can afford to move here. As long-time residents sell their homes, more and more affluent people buy them, gentrifying neighborhoods. Low vacancy rates cause landlords to raise rents, forcing people to look for housing elsewhere. "Thank you essential workers" signs should read "Thank you essential workers, but sorry, Ann Arbor is full. Hope your commute isn't too long!". If you disagree that this is a problem, you might as well stop reading now, because the rest of this is premised on agreeing on this fact.

That brings us to the question: how do we solve this problem? Sometimes I'm not sure if people are kidding, but I have heard people suggest that we stop building more housing in order to make housing more affordable. I'm not sure how that would possibly work without a concerted effort to make Ann Arbor horribly unwelcoming, so clearly doing nothing isn't the answer. Despite the desires of some people on Nextdoor, we can't freeze Ann Arbor the way it was when they bought their house in the 80s. People are gonna move here, we gotta adapt or it'll only be the _most affluent_ who can afford to be our neighbors. It'll also push gentrification further into Ypsi and surrounding communities, and further increase pollution due to longer commutes and more traffic. That's not ok with me.

We have built a [little bit more housing](https://treedowntown.com/2019/11/04/the-boomtown-fallacy/), but housing demand still outstrips supply.  A few "dense projects" here and there isn't going to get us out of this mess, and a history of blocking and delaying viable projects downtown and on transit lines doesn't fill me with confidence that the current council majority has the desire to do what we need to help make Ann Arbor a more diverse and welcoming community. We certainly shouldn't be arguing for years and then in the end not building anything, which is exactly what happened at least 3 times in recent memory, costing taxpayers $5-10M in the process, not including lost tax revenues from new taxpayers.

We also must solve our housing shortage from all angles. I often hear objections to a particular housing plan because it doesn't solve literally every housing problem immediately. Should we instead do nothing? Of course not! We need more market rate housing, but market rate housing is not sufficient. We need more subsidized housing, but subsidized housing is not sufficient. We need creative solutions, vision, and flexibility to try new things. Sure, use the process to make projects better, but concern trolling a project or idea until it goes away (or turns into 16 by-right McMansions instead of hundreds of denser homes) as we have seen recently is _not the way_. We spend far too much time getting to "no" or making projects worse rather than working together to _efficiently_ and _productively_ get to "yes".

We need to decrease barriers to building dense housing, and we need to fund affordable housing, and we're way behind the ball. We need more housing of all kinds, _except_ single family housing. There's effectively no more room for single family homes, and it's the least efficient use of the scarce land we have left inside the city.

I know there are folks around town who are trying to scare everyone about density, but don't be afraid. They're threatening you with new neighbors, friends, colleagues, classmates, and more.  I'm excited to welcome them to our community.

# Who can help?

Luckily we have great candidates who are willing to fight to make Ann Arbor a better place for all. Someone who convinces me they will work in good faith towards moving Ann Arbor forward will have my vote and support. To that end, I recommend voting for [Lisa Disch](https://www.votedisch.com/) in Ward 1, [Linh Song](https://songfora2.com/) in Ward 2, [Travis Radina](https://www.travisradina.com/) in Ward 3, [Jen Eyer](https://www.votejeneyer.com/) in Ward 4, and [Erica Briggs](https://www.ericafora2.com/) in Ward 5. They are the leaders we need in Ann Arbor to make progress on our housing crisis before it gets even worse.

Do these candidates agree with each other all issues under all circumstances? Do I agree with them on all issues? Of course not! But I trust them to act thoughtfully and diligently to make decisions in the best interest of the residents of Ann Arbor and our future neighbors. I trust them to have sound judgement and good values. They each bring skills and experience that will serve Ann Arbor well. They're who we need on council.

In this post I have mostly spoken about housing, because it's one of the things that is a particularly big problem that can be addressed by local government. There are obviously other issues that matter, and I also think that these candidates are well suited to act on them as well: clean water, policing, safe streets, and equity issues to name a few. For the same reason I expect that they will make good housing decisions, I trust their thoughtfulness and problem solving in other matters that matter to our community.

Please vote for a better future for Ann Arbor.

# A note about voting in a pandemic

Election day is Tuesday, August 4th. If you haven't returned your absentee ballot yet, you **must** return it yourself, to the City Clerk's office. [Track your ballot](https://mvic.sos.state.mi.us/) and if it's not there by Monday, go to City Hall before they close. If it's not registered by the time the clerk's closes on Monday, it's my understanding that you have to go to your polling place and they can help you. I'm not 100% sure about that, but contact the clerk or show up at your polling place to ask a poll worker. If you have better details, let me know and I will update this post.
