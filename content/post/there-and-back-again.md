+++
title = "There and Back Again - My 2-year foray into management"
date = 2018-01-16T22:47:52-05:00
draft = true

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["management"]
categories = []

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++

I've spent the last 2 years as (the only) Director of Engineering at Olark.  For the most part, this means I was responsible for management across our ~13-member engineering organization, directly managing 4-5 people and dealing with policies and procedures involving engineers.  Prior to that, I had been an engineer, consultant, technical cofounder, and tech lead at various different places, occasionally doing management tasks, but certainly not as a matter of course.

After ____ my role will be changing to ____, focusing primarily on Olark's organization-wide technical direction.  This was previously part of my role as Director, but the day-to-day demands of management prevented me from devoting the uninterrupted time necessary to make significant progress.

The last couple years have been incredibly educational and I'm very glad I had the opportunity to explore this role, but some time ago it began to occur to me to me that, while I neither hate nor am bad at management, I am much more passionate about technical work than management.  I had brief periods of technical contribution that were exciting, but those were few and far between.  The trouble is, I didn't _hate_ my job, so I muddled on, only half satisfied.  Fully realizing that I needed to get out of a management position took me quite some time and frank conversations with others, including my wife, friends, colleagues, and Suzan Bond, who I can't recommend highly enough if you're at a similar decision point in your career.

Luckily, Olark is an incredibly supportive organization (as I knew they would be) and reacted with a sincere desire to find a role that will suit both my needs and the needs of the company.  I think this is rare in the industry, though.  A lot of my friends who had similar realizations felt like they had to find a position at a new company instead of being able to change roles within their company.  Whether that was perception or fact, I think organizations lose a lot of good people by not making it explicitly easy for employees to transition between technical and management tracks.  I, however, am incredibly grateful that I am able to continue my journey at Olark.

I'm not sure what the future holds, but I'm excited to be back in the trenches.