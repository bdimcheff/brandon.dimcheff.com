---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "What's the Deal With Hayner"
subtitle: ""
summary: "A chronological description of Ann Arbor Councilmember Jeff Hayner's problematic behavior over the last few years."
authors: []
tags: []
categories: []
date: 2021-06-08T23:26:10-04:00
lastmod: 2021-06-08T23:26:10-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
I've had a number of people, both local and afield, ask me "what's the deal with this Jeff Hayner guy?" This post attempts to give you a short, chronological summary of what "the deal" is, as of June, 2021.

**CW**: there are redacted slurs in here, some links contain unredacted versions which I have noted.

**tl;dr**: Hayner was and is an edgelord. Edgelords don't make good elected officials because posting reactionary garbage online is not a good way to get your constituents to trust and work with you. Sometimes people grow into their positions, but he shows no sign of becoming less of an edgelord. He's become unhinged enough lately to blow past the line of acceptable behavior. He should resign or be [recalled](https://haynerrecall.com).

------

First, a disclaimer: I have never thought that Jeff was a good choice for an elected official, for reasons that will hopefully become clear. I also am helping with his recall campaign. I wish I didn't have to, as it's much nicer to work to make something better than it is to fight against someone who's causing problems on purpose. I am not Totally Unbiased™, nor am I attempting to be. I am, however, going to tell you the truth to the best of my ability. The truth is, Jeff Hayner simply isn't qualified to be a public official.

------

Second, an interlude: dealing with this doesn't make anyone's life better, but it is table stakes. We need to stop Hayner from making Ann Arbor inhospitable. We can't simply ignore his behavior. He has forced our hand.

In no way does addressing this even remotely "solve" racism or homophobia in Ann Arbor. Aura Rosser's killer remains on the police force, and barely any new police accountability has been implemented. Ann Arbor has gotten significantly whiter over the last 20 years. We must do better in substantive ways.


## Pre-council Activity

Jeff ran for council in 2013 as an Independent against Sabra Briere, but lost. Afterwards, he was fairly active on [#a2council](https://twitter.com/search?q=%23a2council&src=typed_query) since at least 2014, often taking a role best described as [edgelord](https://www.dictionary.com/e/slang/edgelord). Once it became apparent he was going to run in 2018, somebody [archived his tweets](http://www.haynertweets.com/) to preserve them, so voters could see the kind of posts he was making. He filed a DMCA takedown on that archive (which required him to certify under penalty of perjury that the tweets were his) but there is a [mirror](https://jeffhaynertweets.com/) that remains active today.

There are some people who seem to think that it's weird or out of line to have archived a prospective candidate's social media posts, but that seems like fair game to me. Nobody forced Jeff to post these things, nor did they force him to run for office. I personally thought his posts would be enough to turn off voters and keep him from getting elected, but it didn't turn out that way.

A little taste: he calls a council member (who he now serves with) "RBF". He talks about "snowflakes" being "triggered" and complains about "SJW"s. Classy stuff.

## 2018 Primary

In Ann Arbor, once you've won the Democratic primary, you're all but assured to win the general election. Jeff won the primary over Ron Ginard by 130 votes. There were [shenanigans](https://annarborobserver.com/articles/the_education_of_ron_ginyard.html) in the process, but that is a different blog post. Jeff deleted his twitter account during the campaign, and has mostly posted in the [Ann Arbor Politics](https://www.facebook.com/groups/178850790201740) Facebook group instead.


## Early Council

For the first two years of his term on council, Jeff's [faction](https://samfirke.com/2020/06/29/the-factions-of-ann-arbor-city-politics-and-why-theyre-a-problem/) was in control of council, though I think Jeff has been a bit of a wildcard generally. He's often a lone dissenting vote on things like the budget, forming his own grumpy caucus. He definitely posted some garbage, but his allies were in control of council which I think both moderated the vitriol and his personal persecution complex.

The 2018-2020 councils also didn't get much done. It turns out that a bunch of people who are just mad about stuff aren't that good at productively governing, and they mostly spent their time firing the city administrator and dealing with other messes that they created. The lack of good governance also made a lot of people in Ann Arbor start paying more attention to local politics.

That didn't stop him from posting questionable stuff to social media, though.

<!-- insert bike lane thing -->

## 2020 Election

In the 2020 primary, the Strivers/Council Party/YIMBYs/The Mayor's Evil Cabal _handily_ dominated the ballot. It was not even close in any of the five races, some of them being 2:1 margins of victory. Hayner went from being allied with the majority to being in the minority. His history of seeing conspiracies and corruption everywhere are not compatible with being in the minority, and his behavior worsened.

## Hayner's Stairs

I think the real precursor to this episode of unhingedness was press coverage of his [illegal addition](https://www.mlive.com/news/ann-arbor/2020/12/ann-arbor-city-council-member-fined-for-adding-second-floor-to-home-without-permit.html) in late 2020. It turns out he had built an entire second story on his house without pulling permits. If he wasn't an elected official, he'd just be a random dude who didn't pull permits, but imo it's different when you make the rules.

{{<tweet 1334665894421819393>}}

Now, Hayner claims that he never finished his addition because ran out of money, and there aren't any stairs going up to his second story, so therefore it didn't count as _tax evasion_, just lack of permits. I have no idea if that is an actual argument, but it seems suspect. I assume this is all still working itself out, but if you see memes or comments about Hayner's stairs, that's what people are referencing.

Anyhow, Hayner didn't like this coverage. He claims that if the reporter had taken a photo from a different angle, you would see the permits in his window. This may be true, as he has applied for permits retroactively, and he did so prior to the article being published. But his house seems to have had the addition for years before he applied for the permits, at least on google maps. It's possible he'll be liable for back taxes on the addition's value for that time, but I'm not an expert.

This press coverage seems to have made Hayner feel personally persecuted, or exacerbated his already paranoid view of them. If you look in the [tweet database](https://jeffhaynertweets.com/), you'll see some evidence of him having a long-running grudge (search for "stanton").

As an aside: clever internet sleuths figured this out because he continuously complains about living in a tiny house, and someone finally went by his house (his address is publicly listed on the city's website) and noticed that it looked not that small. It also didn't match the specs in the city's property tax database. Oops.

## The Trigger

Somebody posted [Letter from the Editor: It’s time for reasonable people to push hate-filled trolls back under the bridge
](https://www.mlive.com/news/2021/04/letter-from-the-editor-its-time-for-reasonable-people-to-push-hate-filled-trolls-back-under-the-bridge.html) to [Facebook](https://www.facebook.com/groups/178850790201740/permalink/465021974917952/). Hayner responded thrice:

> Pretty rich coming from John Hiner. MLive feeds the frenzy. They posted a picture of my house for no reason, if the photographer would have walked 10 feet north and taken the picture they could have shown my unfinished house, with permits in the window. Instead they selected an image designed to make it look finished, the suggestion was I was dodging taxes. this lead to all sorts of harassment of me and my family. MLive knows what it's doing, they want controversy, it drives clicks.

> "journalists"

and

> "The press is a gang of cruel f*****s. Journalism is not a profession or a trade. It is a cheap catch-all for fuckoffs and misfits–a false doorway to the backside of life, a filthy piss-ridden little hole nailed off by the building inspector, but just deep enough for a wino to curl up from the sidewalk and masturbate like a chimp in a zoo-cage."
>
> -HST

The quote was up for at least 16h, and was eventually deleted by Facebook after someone reported it as violating the TOS.

## The Aftermath

People were rightfully mad about this once it became widely known. A councilmember posting a homophobic slur to attack the press for coverage he doesn't like is absurd, and is not reasonable anywhere, let alone a city that prides itself on being inclusive and welcoming. Yes, it's from a Hunter S. Thompson quote, but he _chose the quote_ because he was _mad at a journalist_ about _coverage of him_. It's not noble in any way.

There are those that defended and continue to defend him, anywhere along the spectrum of "he did nothing wrong" to "we don't like that but the process isn't being followed properly so we're powerless to intervene". Most, however, seem to find his behavior unacceptable enough that they're willing to allow city council to condemn his behavior without objection.

As I mentioned, I am helping with a [recall campaign](https://haynerrecall.com). There is more info there, including a list of all the media coverage that we've found.

The daily has [excellent coverage](https://www.michigandaily.com/ann-arbor/ann-arbor-councilmember-jeff-hayner-defends-posting-homophobic-slur-after-criticism-from-fellow-councilmember/) of this part (CW: they don't redact the slur) of stuff leading up to 4/12, but here's a brief summary and some things that happened since then:

* 4/8 CM Hayner posts the above quote containing a homophobic slur.
* 4/12 The Daily publishes the above article, where Hayner refused to apologize.
* 4/13 Hayner publishes an "apology": "I acknowledge the language I quoted is offensive, recognize my poor judgement in using it, and I sincerely apologize for the harm I have caused the community"
* 4/13 [Council Administration Committee](http://a2gov.legistar.com/MeetingDetail.aspx?ID=851650&GUID=649C2FB5-65E8-481A-9ECA-D9400EFEDEAE&Options=info|&Search=) votes to propose stripping CM Hayner of his committee assignments in response to his behavior. CMs Eyer, Grand, Ramlawi and Mayor Taylor support this measure, Griswold opposes it in favor of some sort of general council counseling thing. At some point after this meeting CM Ramlawi decides to flip his vote, and becomes a fervent Hayner defender. At this point it's "partisan" and that bell is unlikely to be un-rung.
* 4/19 Council meeting: [DC-5](http://a2gov.legistar.com/LegislationDetail.aspx?ID=4916678&GUID=8AAA581A-F949-4A5B-8CEA-824FB51F5656&Options=&Search=): Council strips CM Hayner of his committee assignments, but not unanimously. CMs Ramlawi and Griswold voted against it, and CM Nelson put up a stink but eventually voted in favor. He doesn't appear to express any contrition during the process, and I doubt he was ever sorry.
* 5/13 Hayner says the n-word twice in an interview with an mlive reporter while again defending his posting of the original homophobic slur. At this point it was obvious that he wasn't sorry:

{{< tweet 1392607838552432641 >}}

* 5/17 Council meeting: Various council members address Hayner's escalating bad behavior, but not via resolution. Theoretically disciplinary motions need to go through Admin committee, which didn't meet between 5/13 and 5/17.
* 5/28 A [FOIA](https://a2docs.org/view/575) of council emails related to Hayner's original post is released. It contains some wild things, including a very bizarre reply to some kind of MAGA troll on p179. Oddly the email I sent to council was not in there.
* 6/2 Special session of [Admin Committee](http://a2gov.legistar.com/MeetingDetail.aspx?ID=829770&GUID=EB65E3A1-9815-4BAF-A2F4-0BBA729492CB&Options=info|&Search=) called to address Hayner's behavior. There are 2 options: ask Hayner to resign or start a "Rule 12" process (which as far as I know has never been used before).  CMs Ramlawi and Griswold object to the "ask Hayner to resign" option on "due process" grounds. Note that it's been 2 months since Hayner first posted the homophobic slur.
* 6/7 Council meeting: [DC-6](http://a2gov.legistar.com/LegislationDetail.aspx?ID=4971949&GUID=27CAD262-73C7-4BAE-8D19-5A4E082C3442&Options=&Search=) passes at 1:30am after an extremely long meeting, [asking CM Hayner to resign](https://www.michigandaily.com/news/city-council-discusses-the-hayner-recall-early-leasing-housing-proposal-valhalla-ann-arbor-site-plan/). The city attorney clarifies that this is not a due process violation, as Council has a right to speak (by asking Hayner to resign) as much as Hayner has a right to say whatever garbage comes into his mind. CM Ramlawi spent some time objecting to the resolution, acknowledging that he's likely to suffer political fallout for defending Hayner. In the end, Ramlawi and Nelson voted against this _purely symbolic_ resolution, and CM Griswold had left the meeting due to being ill.
* 6/8 It's been 2mo since CM Hayner's original post. He's walked back his brief apology by continuing to escalate his poor behavior. There's no way he's sorry. I don't think he has the ability to accept responsibility for his actions. I do not look forward to seeing what's next. He should resign.

Please Jeff, for the good of Ann Arbor, resign.
