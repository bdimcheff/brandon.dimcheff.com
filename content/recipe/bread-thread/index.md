---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "#BreadThread"
subtitle: "or: how Brandon makes bread these days"
summary: "I set on a mission to make some good, not-too-sour sourdough that I was allowed to eat. I ended up getting so much more out of it than that."
authors: []
tags: ["recipe", "bread", "sourdough"]
categories: ["Recipes"]
date: 2019-12-28T11:34:52-05:00
lastmod: 2020-03-29T16:33:12+0000
featured: true
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: "christmas loaf"
  focal_point: "Top"
  preview_only: false
  alt_text: "a loaf of bread with a box score and a christmas tree drawn on it"

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

# A little bit of history

I've been making bread off and on since high school, with varying degrees of success. We had a bread machine during the bread machine craze, which got pretty good use through college. I graduated to no-knead bread in a dutch oven maybe 10 years ago after it got popularized by the New York Times and Cooks Illustrated. All of that bread was _fine_ (certainly better than grocery store bread) but it was never _great_. The no-knead recipe was a relatively low-effort way of getting fresh-baked bread, but it just became something I did when I needed bread and didn't really think much about it.

About a year ago, I ended up in a medical study (that's a different post) that allowed me to eat somewhere around a half a slice of sandwich bread _unless_ it was sourdough. Well in my opinion, Brandon deserves more than a little bread, as a treat. The trouble is, I don't like sourdough. Or at least I didn't think I liked sourdough. But I'd rather eat 2 slices of bread rather than a half of a slice so... I bought some sourdough from the store. It was meh, as expected. Too sour. Then I wandered into Zingerman's and discovered that the farm bread is actually 100% naturally leavened, aka sourdough. I genuinely like farm bread, so what was going on here? I set on a mission to make some good, not-too-sour sourdough that I was allowed to eat. I ended up getting so much more out of it than that.

# What is sourdough, anyways?

Sourdough is just bread that's made with naturally occurring yeast and bacteria instead of commercially produced yeast. At its most basic, it contains only three ingredients: water, flour, and salt. Of course, you have to grow your own little culture of microorganisms in a flour/water slurry, but there's no added yeast.

At this point, I've gotten everything streamlined well enough that it only really takes me 45min of wall time to actually make 2 loaves of bread, assuming I'm not trying out a new recipe and everything goes well. I could probably trim it to 30min if I really was ambitious, but I find the process relaxing and I'm not trying to be as efficient as possible.

* 5min to weigh everything out and do the initial autolyze mix
* 5-10 min to do the final mix
* 30s ✖️ 3-4 to stretch and fold the dough
* 10min to shape
* 10min to score and bake

# #BreadThread

First, the fun part! I spent some time over the holidays writing a stream-of-consciousness thread while I was preparing and baking bread. It contains some puns and silliness, but well written it is not.

Below is my original #breadthread in all its glory, and [here's](https://threadreaderapp.com/thread/1208441378751037440.html) the unrolled thread, which is probably easier to read. I'm bready to answer all your questions, so feel free to ask.

{{< tweet 1208441378751037440 >}}

## Other Sourdough Posts

* All about [Starters]({{< relref "sourdough-starter" >}}): starting them, maintaining them, talking to them.

_More to come!_ In the mean time:
* read [The Perfect Loaf](https://www.theperfectloaf.com/)
* read Ken Forkish's [Flour, Water, Salt, Yeast](https://kensartisan.com/flour-water-salt-yeast). Note that I don't follow the Forkish sourdough instructions, or do the upside-down proofing, but it has good general bread technique.

### Buy some supplies

#### Absolutely mandatory

The 3 ingredients that you can't do without:

* Flour: unbleached always. I have King Arthur All Purpose flour, Bread Flour, and Hard Red Winter Whole Wheat around at all times. Some rye is nice from time to time. White rice flour is handy for dusting baskets.
* Water: don't buy it, but you need water without chlorine, so get a filter or fill a pitcher and leave it overnight.
* Kosher or sea salt without additives: Diamond Crystal is good. Get fine sea salt, you gotta dissolve this in room temperature water. No iodine!

#### Really quite necessary

You really gotta have these for bread baking, regardless of sourdough or not:

* [A scale](https://smile.amazon.com/Kitchen-Scale-Bakers-KD8000-Weight/dp/B00VEKX35Y): You need at least 3kg of capacity to make a decent sized batch. It is extremely hard to bake consistently without a scale. Measuring flour by volume is folly. Get a crappy scale before you decide to go without.
* Mason jars: I just use wide-mouthed Ball pint jars. Some larger batches of starter might need larger jars (or using a liquid measuring cup). Plastic lids are also handy.
* Bench knife: for cutting and moving dough. Most doughs are pretty wet, so it's hard to do with your hands.
* Bulk fermentation container: I use a [Cambro](https://smile.amazon.com/gp/product/B002PMV77G) 6L. Round and plastic is good.
* Dutch Oven: You really need a way to steam your bread for the first 20 minutes of cooking if you want it to come out with great crust. A [Lodge Combo Cooker](https://smile.amazon.com/Lodge-L8DD3-Cast-Iron-Dutch/dp/B000LEXR0K) is the best solution because you can use it upside down. A regular dutch oven will work in a pinch. You can also steam your oven, but it's a pain in the butt.

#### Optional but great

* Lame: You need a really sharp blade to score bread well. You can use a sharp knife in a pinch, but a lame makes it easier. I like [this one](https://smile.amazon.com/Bread-Lame-Designed-Stainless-material/dp/B079G7WC79) because it's heavy and easy to switch between straight and curved.
* Bannetons: These [little baskets](https://smile.amazon.com/gp/product/B07F6TWB8W) do a great job cradling your loaves as they proof, and they also impart some cool designs on them if you use them without the liner. The one I linked has real linen liners, which is a plus.
* Dough proofer: I love my [Brod & Taylor proofer](https://brodandtaylor.com/folding-proofer-and-slow-cooker/). An oven with the light on works in a pinch, but this thing will help with consistency, and won't occupy your oven 24/7.
* Bowls that fit your bannetons: I have large glass pyrex bowls that my bannetons fit inside entirely. I put plastic wrap on them and stick them in the fridge overnight, which I find more convenient than messing with plastic bags. A plastic container with a lid would also probably work nicely.

For more, check out [Maurizio's list](https://www.theperfectloaf.com/my-baking-tools/).

