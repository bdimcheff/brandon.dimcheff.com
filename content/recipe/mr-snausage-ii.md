+++
title = "Mr. Snausage II"
date = 2017-11-12T16:42:11-05:00
draft = false

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["recipe", "pressurecooker", "pasta", "sausage", "bellpepper"]
categories = ["Recipes"]

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++

This recipe is a common quick dinner for us.  It's quite easy if you have an electric pressure cooker.  It's adapted from a [recipe](https://www.cooksillustrated.com/recipes/7536-pressure-cooker-easy-ziti-with-sausage-and-peppers) from Cooks Illustrated.  You can use any sort of non-strand pasta for this, like shells, ziti, rigatoni, etc.  Spaghetti and strand pasta will create a disaster.

Why Mr. Snausage II?  Mr. Snausage I was the older recipe I used, but this one is easier.  I'll post the other one eventually.

## Special Equipment
I make this in my 8 quart [Instant Pot IP-DUO80](http://amzn.to/2yzkplO), but it should work in a 6 quart also.

# Ingredients

* 1 T olive oil
* 1 lb sweet (or hot if you want it spicy) Italian sausage, casings removed
* 1 onion, chopped fine
* 2 bell peppers, stemmed, seeded, and cut into 3/4-inch pieces
* 1 (25 oz) jar tomato sauce
* 1 jar plus 1/2 c water
* 1 lb large shell pasta (or another pasta you prefer)
* 2 T chopped fresh basil
* 4 oz mozzarella cheese, shredded
* Salt and pepper 

# Method

1. Heat 1T olive oil in your pressure cooker until shimmering.
1. Add sausage, onion, bell pepper, and 2t kosher salt.
1. Cook, breaking up sausage into 1/4" pieces with a wooden spoon, until the sausage isn't pink anymore.
1. Add jar of pasta sauce, refill it with water and add the water as well, using the water to rinse the remainder of the sauce out of the jar, plus an extra 1/2 cup of water.
1. Add the pasta and mix well.
1. Set the pressure cooker for 5 minutes on high, and seal the pot.  I frequently take the lid off and stir once before the mixture starts boiling too much, just to make sure it doesn't stick.
1. Once the timer finishes, quick release your pressure cooker.
1. Remove the lid and stir for a couple minutes to unstick the pasta and thicken the sauce.
1. Add chopped basil and shredded cheese, stirring gently to melt the cheese into a creamy sauce.
1. Serve.

# Leftovers

This actually reheats relatively well.  The pasta gets a little bit soft, but there's not much you can do about that.  Just add a couple teaspoons of water per serving and microwave it, stirring a couple times, until hot.

