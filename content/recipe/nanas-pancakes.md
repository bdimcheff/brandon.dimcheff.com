+++
title = "Nana's Pancakes"
date = 2017-11-12T15:52:39-05:00
draft = false

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["recipe", "breakfast"]
categories = ["Recipes"]

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++

This is the family recipe we've been using since I was a child.  This will make approximately 6 large pancackes, or 12 small ones.  Nana used to let me eat the butter when my mom wasn't watching, which I also highly recommend.

**Note:**  Do not flip the pancakes more than once.  Do not whack them with your spatula.  Do not shake the pan around.  You just spent a lot of time beating some egg whites so the pancakes are light and fluffy.  Don't ruin it.

# Ingredients

* 2c (9oz) all purpose flour
* 1T sugar
* 0.5t salt
* 2t baking soda
* 2.5c buttermilk
* 2 eggs, separated
* 1T melted butter
* 0.5t vanilla extract

# Method

1. Mix dry ingredients together in a bowl
1. Put buttermilk, egg yolks, vegetable oil, and vanilla into a separate bowl.  Whisk until well mixed.
1. Beat egg whites until stiff
1. Add buttermilk mixture to dry ingredients.  Mix with whisk until just incorporated.  It is ok if a few lumps still exist.  Overmixing will make the pancakes tough.
1. Fold stiff egg whites into batter until just incorporated.  It is ok if a few blobs of egg white still remain unincorporated.
1. Preheat heavy bottomed (I use cast iron) skillet over medium heat.
1. Spray skillet with cooking spray or grease with a little butter.  Ladle approximately 1/2c of batter onto the skillet.  Distribute the batter with the back of a ladle moving in a circular motion until the pancake is about 6 inches around.
1. Reduce heat to medium-low and cook for 2-4 minutes, until the edges of the pancake is set and at least one bubble that has formed leaves a small hole in the pancake when it pops.  Flip with a wide spatula.
1. Cook for another 1-2 minutes, until the bottom is golden brown.
1. Serve with your favorite pancake topping.  I like a mixture of apple butter and maple syrup.
