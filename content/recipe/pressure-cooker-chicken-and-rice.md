+++
title = "Pressure Cooker Chicken and Rice"
date = 2017-11-13T17:48:50-05:00
draft = false

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["recipe", "chicken", "rice", "pressurecooker", "cheap"]
categories = ["Recipes"]

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++

This is a modified version of a [recipe](https://www.hippressurecooking.com/pressure-cooker-chicken-and-rice-one-pot-meal/) from Hip Pressure Cooking that I make quite frequently.  It's pretty fast and _very_ cheap.  I recommend an [Instant Pot IP-DUO80](http://amzn.to/2yzkplO) if you don't have a pressure cooker.  You can probably only fit 8 legs in a 6-quart cooker, but an 8 quart should hold 12.

# Ingredients

* 1 T olive oil
* 1 onion, chopped
* 1 T tomato paste
* 1 clove garlic, minced
* 1 bay leaf
* 1.5 T kosher salt
* 1/2 t dried thyme or a couple sprigs fresh
* 1/2 cup water (or your pressure cooker's minimum required liquid amount)
* 8-12 bone-in, skin-on dark meat chicken pieces (about 2-3lb)
* 2 cups (340g) basmati rice

# Method

1. Heat oil on high saute mode until shimmering.
1. Add onion and cook for a few minutes until softened but not browned.
1. Add garlic and cook, stirring constantly, for 30 seconds until fragrant.
1. Add tomato paste and cook, stirring constantly, for 30 seconds until fragrant and beginning to caramelize a little.
1. Turn off pressure cooker, add 1/2 cup water, and scrape up any browned bits.
1. Add bay leaf, thyme, chicken, and salt, and mix everything around a bit and then nestle the chicken down into the pot.  Try to get a fair amount of salt onto the chicken itself.
1. Set the pressure cooker for 9 minutes on high and secure the lid.
1. After the time is up, let the pressure cooker sit for 5 additional minutes and then release the remaining pressure.
1. Remove the chicken pieces to a plate and tent with foil.
1. Pour the liquid in the pot into a 1-quart measuring cup.
1. Add water until the line between the water and fat is at the 3 cup line.  Don't skim the fat off :-o
1. Return the liquids to the pot and add the rice.
1. Set the cooker for high for 2 minutes.
1. Once the timer is up, let the pressure release for at least 10 minutes, then release the remaining pressure and remove the lid.
1. Stir a few times gently, then serve with the chicken.
