---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Pressure Cooker Red Beans and Rice"
subtitle: ""
summary: "A popular Louisiana creole dish, ruined by a yank."
authors: []
tags: ["recipe", "pressurecooker"]
categories: ["Recipes"]
date: 2020-11-26T14:09:26-05:00
lastmod: 2020-11-26T14:09:26-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false
  alt_text: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Adapted from Cooks Illustrated, Alton Brown, and for a pressure cooker.

# Ingredients

* 1 pound small red beans (about 2 cups), rinsed and picked over
* 4 slices bacon (about 4 ounces), chopped fine
* 1 medium onion, chopped fine (about 1 cup)
* 1 small green bell pepper, seeded and chopped fine (about 1/2 cup)
* 1 celery rib, chopped fine (about 1/2 cup)
* 3 medium garlic cloves, minced or pressed through garlic press (about 1 tablespoon)
* 1 teaspoon fresh thyme leaves
* 1 teaspoon sweet paprika
* 2 bay leaves
* ¼ teaspoon cayenne pepper
* Ground black pepper
* 4 cups low-sodium chicken broth
* 12oz pickled pork (see [alton brown's](https://www.foodnetwork.com/recipes/alton-brown/pickled-pork-recipe-1943188) recipe)
* 8 ounces andouille sausage, halved lengthwise and cut into 1/4-inch slices (see note)

# Method

1. Dissolve 3 tablespoons salt in 4 quarts cold water in large bowl or container.
1. Add beans and soak at room temperature for at least 8 hours and up to 24 hours.
1. Drain and rinse well.
1. Heat bacon in pressure cooker set to high, stirring occasionally, until browned and almost fully rendered, 5 to 8 minutes.
1. Add onion, green pepper, and celery; cook, stirring frequently, until vegetables are softened, 6 to 7 minutes.
1. Stir in garlic, thyme, paprika, bay leaves, cayenne pepper, and 1/4 teaspoon black pepper; cook until fragrant, about 30 seconds.
1. Stir in beans, pickled pork, and broth.
1. Set pressure cooker to 20 minutes on high. Allow pressure to release for 20 minutes naturally, then release the rest.
1. Set pressure cooker to saute low and bring to a boil.
1. Add andouille and cook for about a half hour, stirring occasionally, until beans are thick and creamy.
1. Serve on rice (cook it first)