---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "University of Michigan Dorm Chicken Broccoli Bake"
subtitle: "the best dorm food in the land"
summary: "Dorm food isn't traditionally regarded as good, but this dish is almost everyone's favorite from the dorms at Michigan. Hint: butter and two kinds of cream helps."
authors: []
tags: ["recipe", "chicken", "broccoli", "casserole"]
categories: ["Recipes"]
date: 2020-11-26T14:11:17-05:00
lastmod: 2020-11-26T14:11:17-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false
  alt_text: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Adapted from [The University of Michigan](https://kb.housing.umich.edu/hc/en-us/articles/360018571692-Chicken-Broccoli-Bake-) to simplify it and make it slightly saucier. Go Blue.

# Ingredients

* 1lb broccoli, chopped fresh or frozen
* 1T or so cooking oil
* 1lb Chicken Breasts, diced
* 3T butter
* 1 small onion, diced
* 3T flour
* 1c chicken broth
* 3/4c Sour Cream
* 1c heavy cream
* 1/2t salt
* pepper to taste
* 1/4c grated Parmesan Cheese
* 1/4c Bread Crumbs

# Method

1. Steam broccoli for a few minutes in the microwave or on the stove, undercooked a little.
2. In a relatively deep saute pan, saute the diced chicken in vegetable until browned and mostly cooked through. Set aside.
3. In the pan over medium heat, melt butter and sauté onions until translucent. Add flour and cook, stirring constantly for 1-2 minutes.
4. Add chicken broth slowly, scraping up browned bits and evenly hydrating the roux while you whisk.
1. Once the broth is added, add sour cream and heavy cream.
7. Gently simmer mixture for 5 minutes. Taste and adjust seasoning.
8. Gently fold in the cooked chicken meat and the cooked broccoli.
9. Top with combined bread crumbs and Parmesan cheese. Spray the top with some cooking spray to help the bread crumbs brown.
10. Bake for 20min in a 325-degree oven until the top is lightly browned and mixture is bubbly and the chicken is cooked.
11. Serve over rice.
