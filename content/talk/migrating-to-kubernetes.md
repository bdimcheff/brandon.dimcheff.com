+++
title = "Migrating Legacy Infrastructure to Kubernetes: Olark's Lessons From the Trenches"
publish_date = 2016-11-09T11:45:00-08:00
date = 2016-11-09T11:45:00-08:00
date_end = 2016-11-09T12:25:00-08:00
draft = false

abstract = "When you start building new infrastructure from scratch, it’s relatively easy to take into account the constraints that Docker and Kubernetes impose. Unfortunately most of us aren’t starting brand new projects, but are maintaining and migrating legacy infrastructure that may not be well suited to run on Kubernetes. Over the past several months, Olark has migrated a number of services that were never designed with Kubernetes or Docker in mind from over 200 puppet-managed Ubuntu VMs to Google Container Engine. Brandon will share some of Olark’s successes and failures, so that hopefully you can have a good starting point and avoid making the same mistakes that they did. He’ll also answer some questions like: How can I set up DNS and VPN so that I can route between my legacy infrastructure and Kubernetes services? I have a stateful application, can (or should) I still use Kubernetes? What are some things I can do to reduce the risk involved in a large-scale migration?"

abstract_short = ""
event = "KubeCon"
event_url = "http://sched.co/8K8s"
location = "Seattle, WA"

selected = false
math = false
highlight = true

url_pdf = "http://schd.ws/hosted_files/cnkc16/ab/Migrating%20to%20Kubernetes%20-%20Kubecon%202016.pdf"
url_slides = ""
url_video = "https://www.youtube.com/watch?v=r0nhQwbe8OY"

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++
