+++
title = "Orchestructure - Linkerd at Openly"
publish_date = 2019-04-06T16:32:36-04:00
date = 2019-02-27T18:00:00-05:00
date_end = 2019-02-27T20:00:00-05:00
draft = false

abstract = "A little bit of exploration of Linkerd 2.0: quick intro, plus how to use Linkerd to monitor third-party services as if they were your own."

abstract_short = ""
event = "Orchestructure"
event_url = "https://www.meetup.com/orchestructure/events/259159699/"
location = "Ann Arbor, MI"

selected = false
math = false
highlight = true

url_slides = "https://docs.google.com/presentation/d/1uX7aEm5XVGiAYiEWr4NIyvB0wTH857ib9uIcMfbCckw/edit?usp=sharing"
url_video = "https://www.youtube.com/watch?v=csu11mejrvU"
url_code = "https://github.com/bdimcheff/linkerd-demo"

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++
